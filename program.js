//Guardamos el boton HTML en una variable de JS

let boton = document.querySelector("#login-button");

//Imprimimos por consola el contenido de la variable (boton)
console.log(boton);

//relacionamos el boton con la función
boton.setAttribute("onclick", "validarUsuario()");

//Funcion para validar el nombre de usuario y contraseña
function validarUsuario() {
    console.log("Estamos adentro de la función");
    
    //Este es el usuario registrado
    const USUARIO_REGISTRADO = "Pedro";
    const CLAVE_REGISTRADO = "12345";

    //Estos son los datos ingresados en el formulario
    let miClave = document.querySelector("#pass").value;
    let miUsuario = document.querySelector("#user").value;

    if (USUARIO_REGISTRADO == miUsuario && CLAVE_REGISTRADO == miClave) {
        //Camino verdadero, nos deja pasar
        console.log("Ingresaste");
        
        //Agrega class para que tenga efecto visual
        document.querySelector("#contenedor").className += " form-success";

        //Oculta el formulario
        document.querySelector("form").style.display = "none";
    } else {
        //Camino si validación da falso, no nos deja pasar
        console.log("Afuera!");

        //Comentario para generar cambio
    }
} 